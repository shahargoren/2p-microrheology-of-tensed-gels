from collections import namedtuple

import cv2
import numpy as np
import matplotlib.pyplot as plt
import pickle
from fourier_tools import get_amplitude_from_fft
# from tracking_tools import *
import tracking_tools
import json
import os
from PIL import Image
import params
from pathlib import Path
FOLDERS = [Path(r'D:\Shahar\Shahar Optical Tweezers\sample videos')]

DRIVEN_BEAD_TRAJECTORY_FILE_SUFFIX = ' driven bead trajectory.dat'
DRIVEN_BEAD_AMPLITUDE_SUFFIX = ' driven bead amplitude.dat'
MATLAB_DRIVEN_BEAD_TRAJECTORY_FILE_SUFFIX = ' trajectory.csv'
INITIAL_DRIVEN_BEAD_POSITION_FILE_SUFFIX = 'initial position'
TRACER_TRAJECTORY_FILE_SUFFIX = ' tracer trajectories.dat'
AMPLITUDE_FILE_SUFFIX = ' amplitudes.dat'
DRIVEN_BEAD_RADIUS_SUFFIX = ' bead_radius.csv'
IN_PROCESS_SUFFIX = ' trajectories in process.dat'
DONE_SUFFIX = ' trajectories done.dat'
BAD_FILE_SUFFIX = ' not good.dat'
PositionAmplitudesPhases = namedtuple('PositionAmplitudesPhases', 'position amplitude_x amplitude_y phase_x phase_y')
PositionAndAmplitude = namedtuple('PositionAndAmplitude', 'position amplitude')
COMPUTE_TRACER_TRAJECTORIES = False
COMPUTE_DRIVEN_TRAJECTORY_BEAD = False
COMPUTE_AMPLITUDES = True
METHOD = 'trackpy'
FREQ_IDENTIFIER = 'freq'


def analyze_tracer_trajectories(tracer_trajectories, driven_bead_position):
    """ Analyze tracer trajectories to find their amplitude and position relative to driven bead"""
    tracer_position_amplitude = []
    frame_num = max(len(history) for _, history in tracer_trajectories.items())
    for _, history in tracer_trajectories.items():
        frames = [frame for frame, _ in history]
        trajectory = np.array([pt for _, pt in history])
        if len(frames) < params.MIN_IDENTIFICATION_FRACTION*frame_num:
            # tracer_position_amplitude.append(PositionAmplitudesPhases(None, None, None, None, None))
            continue
        if len(frames) < fps / drive_frequency * params.MIN_NUM_CYCLES:
            print('too few samples for fourier')
            # tracer_position_amplitude.append(PositionAmplitudesPhases(None, None, None, None, None))
            continue
        position, amplitude_x, amplitude_y, phase_x, phase_y = get_amplitude_from_trajectory(trajectory, frames)
        tracer_position_amplitude.append(PositionAmplitudesPhases(position - driven_bead_position, amplitude_x, amplitude_y, phase_x, phase_y))
    return tracer_position_amplitude


def choose_indices_for_fft(frames):
    frames = np.array(frames)
    start = np.argmax(frames > start_oscillation+params.MARGIN_TIME*fps)
    if frames[-1] < end_oscillation-params.MARGIN_TIME*fps:
        end = len(frames)-1
    else:
        end = np.argmax(frames > end_oscillation-params.MARGIN_TIME*fps)
    end = end - (end - start) % int(fps / drive_frequency)
    return start, end


def driven_bead_was_poorly_detected(x):
    if np.sum(np.diff(x) ==0) > params.MAX_UNDETECED_DRIVEN_BEAD_FRAMES:
        return True
    else:
        return False


def get_amplitude_from_trajectory(trajectory, frames=None):
    if frames is None:
        frames = np.arange(len(trajectory))
    new_frames = np.arange(frames[0],frames[-1]+1)
    x = np.interp(new_frames, frames, trajectory[:, 0])
    y = np.interp(new_frames, frames, trajectory[:, 1])
    position = np.array([np.mean(x), np.mean(y)])
    start, end = choose_indices_for_fft(new_frames)
    amplitude_x, phase_x = get_amplitude_from_fft(x[start:end], fps, drive_frequency)
    amplitude_y, phase_y = get_amplitude_from_fft(y[start:end], fps, drive_frequency)
    phase_shift = new_frames[start]*2*np.pi*drive_frequency/fps
    phase_x = np.mod(phase_x - phase_shift + np.pi, 2*np.pi)-np.pi
    phase_y = np.mod(phase_y - phase_shift + np.pi, 2*np.pi)-np.pi
    return position, amplitude_x, amplitude_y, phase_x, phase_y


def get_driven_bead_position_and_amplitude(driven_bead_trajectory):
    x = driven_bead_trajectory[:, 0]
    y = driven_bead_trajectory[:, 1]
    position = np.array([np.mean(x), np.mean(y)])
    if driven_bead_was_poorly_detected(x):
        return PositionAmplitudesPhases(position, None, None, None, None)
    else:
        return PositionAmplitudesPhases(*get_amplitude_from_trajectory(driven_bead_trajectory))



def get_sampling_rate(video_file):
    if video_file.suffix == '.avi':
        video_reader = cv2.VideoCapture()
        video_reader.open(str(video_file))
        return video_reader.get(cv2.CAP_PROP_FPS)
    elif video_file.suffix == '.tiff':
        vid = Image.open(str(video_file))
        try:
            for key in vid.tag:
                obj = vid.tag[key][0]
                if isinstance(obj, str) and 'Framerate (Hz)' in obj:
                    d = json.loads(obj)
                    return d["Framerate (Hz)"]
            return FPS
        except RuntimeError:
            return FPS


def save_var(path, variable):
    with open(path, 'wb') as f:
        pickle.dump(variable, f)


def get_tracer_trajectories(video_file):
    print(video_file)
    if video_file.suffix == '.avi':
        tracer_trajectories = tracking_tools.record_tracer_trajectories_trackpy_avi(video_file)
    else:
        tracer_trajectories = tracking_tools.record_tracer_trajectories_trackpy(video_file)
    return tracer_trajectories


def get_driven_bead_trajectory(video_file):
    if video_file.suffix == '.avi':
        driven_bead_trajectory_file = video_file.parent / 'converted_videos' / (
                    video_file.stem + MATLAB_DRIVEN_BEAD_TRAJECTORY_FILE_SUFFIX)
    elif video_file.suffix == '.tiff':
        driven_bead_trajectory_file = video_file.parent / (video_file.stem + MATLAB_DRIVEN_BEAD_TRAJECTORY_FILE_SUFFIX)
    if (params.WORD_SEPERATOR + 'x') in video_file.stem:
        axis = 0
    else:
        axis = 1
    driven_bead_trajectory = np.genfromtxt(driven_bead_trajectory_file, delimiter=',')

    return driven_bead_trajectory, axis


def get_freq_from_filename(video_file):
    i1 = video_file.name.find(FREQ_IDENTIFIER)
    i2 = i1 + video_file.name[i1:].find(' ')
    i3 = i2+1 + video_file.name[i2+1:].find(' ')
    frequency = float(video_file.name[i2:i3+1])
    return frequency


if __name__ == '__main__':

    trajectory_file_suffix = TRACER_TRAJECTORY_FILE_SUFFIX
    amplitude_file_suffix = AMPLITUDE_FILE_SUFFIX

    for folder in FOLDERS:
        if not folder.exists():
            raise RuntimeError('path does not exist')
        video_files = [file for file in folder.rglob('*tiff') if file.parent == folder] + [file for file in folder.rglob('*avi') if file.parent == folder]
        # video_files = list(folder.rglob('*avi'))+list(folder.rglob('*tiff'))
        for video_file in video_files:
            tracer_trajectories_file = folder / (video_file.stem + trajectory_file_suffix)
            in_process_file = folder / (video_file.stem + IN_PROCESS_SUFFIX)
            done_file = folder / (video_file.stem + DONE_SUFFIX)

            if (COMPUTE_TRACER_TRAJECTORIES or not tracer_trajectories_file.exists()) and not (
                    in_process_file.exists() or done_file.exists()):
                try:
                    save_var(in_process_file, IN_PROCESS_SUFFIX)
                except OSError:
                    continue
                tracer_trajectories = get_tracer_trajectories(video_file)
                save_var(done_file, 'done')
                save_var(tracer_trajectories_file, tracer_trajectories)
                os.remove(in_process_file)

            # if no processes are running, delete all "done" flags
        if len(list(folder.rglob('*' + IN_PROCESS_SUFFIX))) == 0:
            for file in folder.rglob('*' + DONE_SUFFIX):
                os.remove(file)


    for folder in FOLDERS:
        video_files = [file for file in folder.rglob('*tiff') if file.parent == folder] + \
                      [file for file in folder.rglob('*avi') if file.parent == folder]

        # compute tracer and driven bead amplitudes
        for video_file in video_files:
            fps = get_sampling_rate(video_file)
            if 'freq' in video_file.name:
                drive_frequency = get_freq_from_filename(video_file)
            else:
                drive_frequency = params.FREQUENCY
            amplitudes_file = folder / (video_file.stem + amplitude_file_suffix)
            if COMPUTE_AMPLITUDES or not amplitudes_file.exists():
                driven_bead_trajectory, axis = get_driven_bead_trajectory(video_file)
                start_oscillation, end_oscillation = tracking_tools.find_oscillation_boundaries(driven_bead_trajectory, axis, int(params.QUIET_TIME*fps))
                if end_oscillation-start_oscillation < fps/drive_frequency*params.MIN_NUM_CYCLES:
                    print('unable to detect beginning and end of oscillation!')
                    start_oscillation = 0
                    end_oscillation = len(driven_bead_trajectory)-1
                driven_bead_position_and_amplitudes  = get_driven_bead_position_and_amplitude(driven_bead_trajectory)
                if driven_bead_position_and_amplitudes.amplitude_x is None:
                    not_good_file = folder / (video_file.stem + BAD_FILE_SUFFIX)
                    save_var(not_good_file, 'bad file!')
                driven_bead_amplitude_file = folder / (video_file.stem + DRIVEN_BEAD_AMPLITUDE_SUFFIX)
                tracer_trajectories_file = folder / (video_file.stem + trajectory_file_suffix)
                with open(tracer_trajectories_file, 'rb') as file:
                    tracer_trajectories = pickle.load(file)

                tracer_positions_and_amplitudes = analyze_tracer_trajectories(tracer_trajectories, driven_bead_position_and_amplitudes.position)
                save_var(driven_bead_amplitude_file, driven_bead_position_and_amplitudes)
                save_var(amplitudes_file, tracer_positions_and_amplitudes)
    1

