This is a concise example of the analysis of 2-point microrheology experiments.

Input to this code are video files. Video file names start with "bead {number} {x or y}" - the bead number is for identification and selection,
and x / y denotes the direcion of oscillations in that video. Each bead number can have 2 videos, 1 for x and one for y.

How to run this example: 

1) 	first running the matlab code: "find_driven_bead.m"
	input the correct path to your directory of vides. you can also input multiple directories.

	This script is better at detecting large beads (>2 um).
	In the beginning, it will request you to mark (for each video), a rectangular around you bead. Make the rectange large enough to detect the bead throughout its motion, but not too large to help the 		algorithm look for it and ignore confusing it with other beads. 
	Then, the script will ask you to click the position of the driven bead, as an initial guess to its position.
	The positions you click will be automatically stored in your directory and used if you run the script again.

	This script outputs 2 more files, one with the bead trajectory and one with the bead radius, to be used by the python code. 

2)	Open a python project and install all requirements with the command "pip install -r requirements.txt"

3)	Next, run "main.py".

	Input your directory\ies by changing the value of constant FOLDERS.

	This code runs over all videos, finds tracers and records their trajectories, and then analyses the trajectories to get the position, amplitude and phase of each tracer.
	These data will be saved in folder.

4)	run "plot_results.py"
	This will create figures showing the displacements as double arrows on top of the image of beads.
	This is just an example of one possible way to plot the resuls.
	