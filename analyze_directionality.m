main_path = 'D:\Shahar\Shahar Optical Tweezers\sample videos';      
compute_nop = true;
plot_histograms = true;
paths = {...
main_path
};
for i = 1:length(paths)
    folder = paths{i};
    files = dir([folder, '*.jpg']);
    for j = 1:length(files)
        nop_file = [folder, files(j).name(1:end-length('.jpeg')), ' nop.csv'];
        if compute_nop || ~isfile(nop_file)
            im = imread([folder,files(j).name]);
            im = im(:,:,2);
            im = imgaussfilt(im);

            [Gx,Gy] = imgradientxy(im, 'prewitt');
            Gmag = (Gx.^2+Gy.^2).^0.5;
            Gdir = atan(Gx./Gy);
            Gmag(Gmag < 0.05*max(max(Gmag))) = 0;
            directions = Gdir(Gmag > 0.05*max(max(Gmag)) & im > 0.4*max(max(im)));

            NOP = mean(cos(2*directions));
            writematrix(NOP, nop_file);
            if plot_histograms
                figure('Renderer', 'painters', 'Position', [10 10 900 300]);
                histogram(directions, 50, 'Normalization', 'probability')
                xticks([-pi/2 0 pi/2])
                xticklabels({'\pi/2','0','\pi/2'})
                xlabel('orientation')
                ylabel('probability')
                box on; 
                set(gca, 'FontSize', 14)
            end
        end
    end
end



