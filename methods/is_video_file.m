function [is_video, type] = is_video_file(filename)
    types = {'m4v','tiff'};
    for i = 1:length(types)
        type = types{i};
        
        if length(filename) > length(type) ...
                && strcmp(filename(end-length(type)+1:end), type)
            is_video = true;
            return
        end
    end
    type = nan;
    is_video = false;
end