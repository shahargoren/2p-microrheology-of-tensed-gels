function [px, py, polarity, r] = find_circle_close_to_position(img, px, py, polarity)
%     img = imgaussfilt(img);
    if strcmp(polarity, 'unknown')
        [bright_circles, b_radii] = imfindcircles(img,[20,40], 'ObjectPolarity', 'bright');
        [dark_circles, d_radii] = imfindcircles(img,[20,40], 'ObjectPolarity', 'dark');
        circles = [bright_circles; dark_circles];
        radii = [b_radii; d_radii];
    else
        [circles, radii] = imfindcircles(img,[20,40], 'ObjectPolarity', polarity);
    end
    ind_min = 0;
    if ~isempty(circles)
        mindist = 20;
        ind_min = 0;
        for i = 1:length(circles(:,1))
            dist = norm([circles(i,1)-px, circles(i,2)-py]);
            if dist < mindist
                mindist = dist;
                ind_min = i;
            end
        end
    end
    if ind_min == 0
        if ~strcmp(polarity, 'unknown')
            [px, py, polarity, r] = find_circle_close_to_position(img, px, py, 'unknown');
        else
            disp('circle not found')
            r = nan;
        end
    else
        px = circles(ind_min,1);
        py = circles(ind_min,2);
        r = radii(ind_min);
    end
    if strcmp(polarity, 'unknown') && ind_min > size(bright_circles,1)
        polarity = 'dark';
    elseif strcmp(polarity, 'unknown') && ind_min <= size(bright_circles,1)
        polarity = 'bright';
    end
