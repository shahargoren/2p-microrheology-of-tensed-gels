function [img] = pretracking_processing(img)

img = imadjust(img, stretchlim(img, [0.002, 0.998]));
img = imgaussfilt(img);

