function [frame] = get_video_frame(video_file, frame_num, type)
%GET_VIDEO_FRAME Summary of this function goes here
%   Detailed explanation goes here
global n_frames
file_path = [video_file.folder, '/', video_file.name];
if strcmp(type, 'm4v')
    video = VideoReader(file_path);
    n_frames = video.NumFrames;
    if frame_num > n_frames
        frame = nan;
        return
    end
    frame = read(video, frame_num);
    if ~ismatrix(frame)
        frame = rgb2gray(frame);
    end

elseif strcmp(type, 'tiff')
    if frame_num > n_frames
        frame = nan;
        return
    end    
    frame = imread(file_path, frame_num);
end

