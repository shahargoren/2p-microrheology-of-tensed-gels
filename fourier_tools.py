import numpy as np
from params import FREQUENCY, FREQ_RANGE

def get_amplitude_from_fft(x, fs, f_drive=FREQUENCY):
    """ find the amplitude of the input signal at the chosen FREQUENCY"""
    frequencies, amplitudes, phases = perform_fft(x, fs)
    return _get_amplitude(frequencies, amplitudes, phases, f_drive)


def _get_amplitude(frequencies, amplitudes, phases, frequency_of_interest):
    if frequency_of_interest > max(frequencies):
        raise RuntimeError("frequency of interest is out of fft frequency range")
    max_idx = np.argmax(frequencies > frequency_of_interest * (1 - FREQ_RANGE)) \
              + np.argmax(amplitudes[(frequencies >= frequency_of_interest * (1 - FREQ_RANGE))
                                        & (frequencies <= frequency_of_interest * (1 + FREQ_RANGE))])

    return 2 * amplitudes[max_idx], phases[max_idx]


def perform_fft(signal, sampling_frequency):
    """
    computes a discrete Fourier transform and returns the frequencies, amplitudes and phases computed
    """
    n = signal.size
    signal_ft = np.fft.fftshift(np.fft.fft(signal))
    if n%2:
        frequencies = np.linspace(-(n-1)/2, (n-1)/2, num=n)*sampling_frequency/n
    else:
        frequencies = np.linspace(-n / 2, n / 2 - 1, num=n) * sampling_frequency / n
    fft_amplitude = np.abs(signal_ft) / n
    phases = np.angle(signal_ft)
    return frequencies, fft_amplitude, phases
