import matplotlib.pyplot as plt
import numpy as np
import pickle
import cv2
from dataclasses import dataclass
from pathlib import Path
from typing import List
import main
from main import PositionAmplitudesPhases
import params

S_FONTSIZE = 12
M_FONTSIZE = 14
L_FONTSIZE = 20
CAPSIZE = 5
LINE_WIDTH = 3
CBAR_LIMITS = np.log10([0.01, 1])
CBAR_TICK_LABELS = ('0.03', '0.1', '0.3')
HFONT = {'family':'sans-serif' }
IMAGE_UPPER_THR = 255
IMAGE_LOWER_THR = 50


@dataclass
class Tracer:
    x: float
    y: float
    amp_x: float
    amp_y: float
    phase_x: float = 0
    phase_y: float = 0
    driven_bead_amp_x: float = 0
    driven_bead_amp_y: float = 0
    driven_bead_phase_x: float = 0
    driven_bead_phase_y: float = 0
    driven_bead_direction: str = 'x'
    driven_bead_radius: float = 0


@dataclass
class TracerList:
    tracers: List[Tracer]

    def __getitem__(self, idxs):
        return TracerList(np.array(self.tracers)[idxs])

    def extend(self, new_tracers):
        self.tracers.extend(new_tracers.tracers)

    def x(self):
        return np.array([tracer.x * params.PIXEL_X for tracer in self.tracers])

    def y(self):
        return np.array([tracer.y * params.PIXEL_Y for tracer in self.tracers])

    def amp_x(self):
        return np.array([tracer.amp_x * params.PIXEL_X for tracer in self.tracers])

    def amp_y(self):
        return np.array([tracer.amp_y * params.PIXEL_Y for tracer in self.tracers])

    def amp_x_normalized(self):
        return np.array([tracer.amp_x / tracer.driven_bead_amp_x if tracer.driven_bead_direction == 'x' else
                         tracer.amp_x / tracer.driven_bead_amp_y for tracer in self.tracers])

    def amp_y_normalized(self):
        return np.array([tracer.amp_y / tracer.driven_bead_amp_x if tracer.driven_bead_direction == 'x' else
                         tracer.amp_y / tracer.driven_bead_amp_y for tracer in self.tracers])

    def phase_x(self):
        return np.array([tracer.phase_x for tracer in self.tracers])

    def driven_bead_radius(self):
        return np.array([tracer.driven_bead_radius for tracer in self.tracers])

    def phase_y(self):
        return np.array([tracer.phase_y for tracer in self.tracers])

    def phase_difference(self):
        return np.array([np.mod(tracer.phase_x - tracer.driven_bead_phase_x + np.pi, 2 * np.pi) - np.pi
                         if tracer.driven_bead_direction == 'x' else
                         np.mod(tracer.phase_y - tracer.driven_bead_phase_y + np.pi, 2 * np.pi) - np.pi
                         for tracer in self.tracers])

    def for_x_oscillation(self):
        return TracerList([tracer for tracer in self.tracers if tracer.driven_bead_direction == 'x'])

    def for_y_oscillation(self):
        return TracerList([tracer for tracer in self.tracers if tracer.driven_bead_direction == 'y'])


def threshold_image(im, thr1: int=IMAGE_LOWER_THR, thr2: int=IMAGE_UPPER_THR):
    im[im < thr1] = thr1
    im[im > thr2] = thr2
    im = 255 * (im-thr1)/(thr2-thr1)
    return im


def add_logarithminc_colorbar(ax, item, label=None, cbar_ticklabels=None):
    if cbar_ticklabels is None:
        cbar_ticklabels = CBAR_TICK_LABELS
    cbar = ax.figure.colorbar(item, ax=ax, fraction=0.046, pad=0.04)
    if label is None:
        if params.NORMALIZE_AMPLITUDES:
            cbar.set_label('Normalized Amplitude', fontsize=S_FONTSIZE, **HFONT)
        elif not params.NORMALIZE_AMPLITUDES:
            cbar.set_label(r'A/[$\mu m$]', fontsize=M_FONTSIZE, **HFONT)
    else:
        cbar.set_label(label, fontsize=M_FONTSIZE, **HFONT)
    cbar.set_ticks(np.log10([float(i) for i in cbar_ticklabels]))
    cbar.set_ticklabels(cbar_ticklabels)
    cbar.ax.tick_params(labelsize=M_FONTSIZE)




def _get_extent(img, origin):
    x1 = -origin[0] * params.PIXEL_X
    x2 = (img.shape[1]-origin[0]) * params.PIXEL_X
    y1 = (origin[1]-img.shape[0]) * params.PIXEL_Y
    y2 = origin[1] * params.PIXEL_Y
    return [x1,x2,y1,y2]


def get_tracer_positions_and_amplitudes(video_file):
    tracer_amplitudes_file = video_file.parent / (video_file.stem + main.AMPLITUDE_FILE_SUFFIX)
    if not tracer_amplitudes_file.exists():
        tracer_amplitudes_file = video_file.parent / (video_file.stem + ' trackpy amplitudes.dat')
    with open(tracer_amplitudes_file, 'rb') as f:
        tracer_positions_and_amplitudes: List[PositionAmplitudesPhases] = pickle.load(f)
    return tracer_positions_and_amplitudes


def get_driven_bead_amplitudes(video_file: Path):
    driven_bead_amplitude_file = video_file.parent / (video_file.stem + main.DRIVEN_BEAD_AMPLITUDE_SUFFIX)
    with open(driven_bead_amplitude_file, 'rb') as f:
        driven_bead_amplitude: PositionAmplitudesPhases = pickle.load(f)
    return driven_bead_amplitude


def get_driven_bead_radius(video_file: Path):
    if video_file.suffix == '.tiff':
        driven_bead_radius_file = video_file.parent / (video_file.stem + main.DRIVEN_BEAD_RADIUS_SUFFIX)
    else:
        driven_bead_radius_file = video_file.parent / 'converted_videos' / (
                    video_file.stem + main.DRIVEN_BEAD_RADIUS_SUFFIX)
    driven_bead_radius = np.genfromtxt(str(driven_bead_radius_file)) * params.MEAN_PIXEL_SIZE
    return driven_bead_radius


def get_video_results(video_file: Path) -> TracerList:
    """
    get a TracerList object storing information on the tracers' position, amplitude and phase
    :param video_file: Path
    :return:
    """
    driven_bead_radius = get_driven_bead_radius(video_file)
    driven_bead_amplitudes = get_driven_bead_amplitudes(video_file)
    tracer_positions_and_amplitudes = get_tracer_positions_and_amplitudes(video_file)

    if params.WORD_SEPERATOR + 'x' in str(video_file):
        direction = 'x'
    else:
        direction = 'y'

    tracers = TracerList([Tracer(x=tr.position[0], y=-tr.position[1],
                                 amp_x=directed_amplitude(tr, driven_bead_amplitudes, 'x'),
                                 amp_y=directed_amplitude(tr, driven_bead_amplitudes, 'y'),
                                 phase_x=tr.phase_x, phase_y=tr.phase_y,
                                 driven_bead_amp_x=driven_bead_amplitudes.amplitude_x,
                                 driven_bead_amp_y=driven_bead_amplitudes.amplitude_y,
                                 driven_bead_phase_x=driven_bead_amplitudes.phase_x,
                                 driven_bead_phase_y=driven_bead_amplitudes.phase_y,
                                 driven_bead_direction=direction,
                                 driven_bead_radius=driven_bead_radius) for tr in tracer_positions_and_amplitudes])
    return tracers


def directed_amplitude(tracer: PositionAmplitudesPhases, driven: PositionAmplitudesPhases, direction: str):
    if driven.amplitude_x > driven.amplitude_y:
        driven_direction = 'x'
        sign1 = -2 * (np.pi / 2 < np.mod(np.abs(tracer.phase_x - driven.phase_x), 2 * np.pi) < 3 / 2 * np.pi) + 1
    else:
        driven_direction = 'y'
        sign1 = -2 * (np.pi / 2 < np.mod(np.abs(tracer.phase_y - driven.phase_y), 2 * np.pi) < 3 / 2 * np.pi) + 1
    if driven_direction == direction:
        sign2 = 1
    else:
        sign2 = 2 * (np.pi / 2 < np.mod(np.abs(tracer.phase_x - tracer.phase_y),
                                        2 * np.pi) < 3 / 2 * np.pi) - 1  # Sign is taking into account flippin the y direction
    if direction == 'x':
        return tracer.amplitude_x * sign1 * sign2
    else:
        return tracer.amplitude_y * sign1 * sign2


def get_first_video_frame(video_file):
    import cv2
    from PIL import Image
    if video_file.suffix == '.avi':
        video_reader = cv2.VideoCapture()
        video_reader.open(str(video_file))
        _, frame = video_reader.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    else:
        video = Image.open(video_file)
        video.seek(0)
        frame = np.array(video)
    return frame


def set_axis_labels_for_images(ax, xlabel=True, ylabel=True):
    if xlabel:
        ax.set_xlabel(r'x [$\mu$m]', fontsize=M_FONTSIZE, **HFONT)
    if ylabel:
        ax.set_ylabel(r'y [$\mu$m]', fontsize=M_FONTSIZE, **HFONT)
    ax.tick_params(axis='both', which='major', labelsize=M_FONTSIZE)


def get_vmax_vmin(cbar_ticklabels=None):
    if cbar_ticklabels is None:
        cbar_ticklabels = CBAR_TICK_LABELS
    vmax = np.log10(float(cbar_ticklabels[-1]))
    vmin = np.log10(float(cbar_ticklabels[0]))
    return vmax, vmin



def scatter_log_quivers_on_frame(frame, x, y, u, v, log_amp, initial_position, title='', ax=None, cbar=True, cbar_ticklabels=None, scale=1):
    vmax, vmin = get_vmax_vmin(cbar_ticklabels)

    if ax is None:
        _, ax = plt.subplots()
    frame = threshold_image(frame)
    ax.imshow(frame, cmap='gray', extent=_get_extent(frame, initial_position))

    quivers = ax.quiver(x, y, u, v, log_amp, cmap="plasma", clim=(vmin, vmax), angles='xy', scale_units='xy', scale=scale, alpha=0.85)
    _ = ax.quiver(x, y, -u, -v, log_amp, cmap="plasma", clim=(vmin, vmax), angles='xy', scale_units='xy', scale=scale, alpha=0.85)

    if cbar:
        add_logarithminc_colorbar(ax, quivers, cbar_ticklabels=cbar_ticklabels)

    set_axis_labels_for_images(ax)
    ax.set_title(title, fontsize=M_FONTSIZE, **HFONT)
    plt.show(block=False)


def overlay_arrows_on_image(video_file, ax=None, cbar=True, cbar_ticklabels=None):
    """
        plot double arrows showing oscillation amplitute and direction, on top of image of beads
    :param video_file: file Path.
    :param ax: matplotlib axes to plot upon
    :param cbar: whether to add a colorbar
    :param cbar_ticklabels:list of strings - ticks for colorbar, also control the min and max color values
    :return:
    """
    frame = get_first_video_frame(video_file)
    driven_bead_amplitude_file = str(video_file)[:-len(str(video_file.suffix))] + main.DRIVEN_BEAD_AMPLITUDE_SUFFIX
    with open(driven_bead_amplitude_file, 'rb') as f:
        driven_bead = pickle.load(f)
    if (params.WORD_SEPERATOR + 'x') in video_file.stem:
        title = r'$\leftrightarrow$ oscillation'
    else:
        title = r'$\updownarrow$ oscillation'
    initial_position = driven_bead.position

    tracers = get_video_results(video_file)

    log_amp = np.log10(np.hypot(tracers.amp_x_normalized(), tracers.amp_y_normalized()))
    # x, y, amplitude_x, amplitude_y, log_amp = add_driven_bead_to_data(x, y, amplitude_x, amplitude_y, log_amp, driven_bead)

    scatter_log_quivers_on_frame(frame, tracers.x(), tracers.y(),
                                                  tracers.amp_x_normalized(), tracers.amp_y_normalized(), log_amp,
                                                  initial_position,
                                                  title=title, ax=ax, cbar=cbar, cbar_ticklabels=cbar_ticklabels,
                                                  scale=params.SCALE_ARROWS)


def plot_displacement_vectors(folder, bead_num):
    """
    plot double arrows showing oscillation amplitute and direction, on top of image of beads
    :param folder: Path, path to where files are stored
    :param bead_num: int, The bead number in order to choose the videos
    :return:
    """

    video_files = list(folder.glob(f'*bead {bead_num}*tiff'))
    assert len(video_files) == 2
    fig1, ax1 = plt.subplots(num=1)
    fig2, ax2 = plt.subplots(num=2)
    axs = np.array([ax1, ax2])
    for video_file in video_files:
        if (params.WORD_SEPERATOR + 'x') in video_file.stem:
            overlay_arrows_on_image(video_file, ax=axs[0], cbar_ticklabels=CBAR_TICK_LABELS)
        else:
            overlay_arrows_on_image(video_file, ax=axs[1], cbar_ticklabels=CBAR_TICK_LABELS)

    # label_axes(axs)
    fig1.set_size_inches(6, 4)
    fig2.set_size_inches(6, 4)
    plt.pause(0.001)
    plt.figure(1)
    plt.tight_layout()
    plt.figure(2)
    plt.tight_layout()
    plt.show()

if __name__ == '__main__':
    folder = main.FOLDERS[0]
    bead_number = 22
    plot_displacement_vectors(folder, bead_number)
