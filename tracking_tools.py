import cv2
import numpy as np
from scipy.spatial.distance import cdist
from scipy import signal as sg
import matplotlib.pyplot as plt
from PIL import Image
import pims
import trackpy as tp
import pandas as pd
import params

MIN_IDENTIFICATION_FRACTION = 0.4
START_FILTERING = 10
MAX_DISTANCE_DIFF = 5
MIN_DISTANCE_DIFF = 1
BEAD_LOCATION_WINDOW_NAME = 'Click on bead'
BEAD_MAX_DISPLACEMENT_IN_FRAME = 10
SIGMA = 3
GAUSSIAN_LENGTH = 10
MIN_POINTS_IN_FIT = 4
MAX_ITER = 5
PEAK_DETECTION_THRESHOLD = 0.5
BEAD_SEARCH_RANGE = 50
LOWER_SATURATION = 0.002
UPPER_SATURATION = 0.998
DRIFT_FIX = True
MIN_BEAD_AREA = 1 / 4 * np.pi * (0.75 / params.MEAN_PIXEL_SIZE) ** 2
TRACER_DIAMETER_IN_PIXELS = 1.5 / params.MEAN_PIXEL_SIZE
MAX_ECCENTRICITY = 0.2
MIN_SIGNAL = 10
LOW_ECCENTRICITY = 0.1
HIGH_SIGNAL = 20
MINMASS = 200
DIAMETER = 25
SEPERATION_FACTOR = 1.2
TRACKPY_SEARCH_RANGE = 5


VIDEO_PATH = r'D:\Shahar\Shahar Optical Tweezers\sample videos\bead 96 x 20210629-145023.tiff'


def record_tracer_trajectories_trackpy_avi(video_file):
    video_reader = cv2.VideoCapture()
    video_reader.open(str(video_file))
    tracers = pd.DataFrame()

    frame_idx = 0
    while True:
        has_frames, frame = video_reader.read()
        if not has_frames:
            break
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        dark_tracers = tp.locate(frame, DIAMETER, minmass=MINMASS, separation=SEPERATION_FACTOR * TRACER_DIAMETER_IN_PIXELS, invert=True)
        bright_tracers = tp.locate(frame, DIAMETER, minmass=MINMASS, separation=SEPERATION_FACTOR * TRACER_DIAMETER_IN_PIXELS)
        current_tracers = pd.concat([dark_tracers, bright_tracers])
        current_tracers = current_tracers.loc[(current_tracers['ecc'] < MAX_ECCENTRICITY) & (current_tracers['signal'] > MIN_SIGNAL) &
                          ((current_tracers['signal'] > HIGH_SIGNAL) | (current_tracers['ecc'] < LOW_ECCENTRICITY))]
        current_tracers['frame'] = frame_idx

        tracers = tracers.append(current_tracers)
        frame_idx += 1
    t = tp.link(tracers, TRACKPY_SEARCH_RANGE, memory=3)
    trajectories = {}
    for i in t['particle'].unique():
        current_traj = t.loc[t['particle'] == i, ['x', 'y', 'frame']]
        trajectories[i] = [(row['frame'], (row['x'], row['y'])) for index, row in current_traj.iterrows()]
    remove_false_tracers(trajectories)
    return trajectories


def record_tracer_trajectories_trackpy(video_file, quiet=True):
    if quiet:
        tp.quiet()
    frames = pims.open(str(video_file))
    if params.CORRECT_VIDEO_TIME:
        for i in range(len(frames._tiff)):
            frames._tiff[i].keyframe.tags["DateTime"].value = '1988 08 03 16:00:00'
    dark_tracers = tp.batch(frames, DIAMETER, minmass=MINMASS, separation=SEPERATION_FACTOR*TRACER_DIAMETER_IN_PIXELS, invert=True, processes=1)
    bright_tracers = tp.batch(frames, DIAMETER, minmass=MINMASS, separation=SEPERATION_FACTOR*TRACER_DIAMETER_IN_PIXELS, processes=1)
    tracers = pd.concat([dark_tracers, bright_tracers])
    tracers = tracers.loc[(tracers['ecc'] < MAX_ECCENTRICITY) & (tracers['signal'] > MIN_SIGNAL) &
                          ((tracers['signal'] > HIGH_SIGNAL) | (tracers['ecc'] < LOW_ECCENTRICITY))]
    t = tp.link(tracers, TRACKPY_SEARCH_RANGE, memory=3)
    trajectories = {}
    for i in t['particle'].unique():
        current_traj = t.loc[t['particle'] == i, ['x', 'y', 'frame']]
        trajectories[i] = [(row['frame'], (row['x'], row['y'])) for index, row in current_traj.iterrows()]
    remove_false_tracers(trajectories)
    return trajectories


def get_total_frame_number(video_path):
    if video_path.suffix == '.avi':
        video = cv2.VideoCapture(str(video_path))
        total = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    elif video_path.suffix == '.tiff':
        video = Image.open(video_path)
        total = video.n_frames
    else:
        total = None
    return total


def get_driven_bead_position(img, prev_position, show_circles=False):
    """
    find the position of the driven bead using Hough transform, requiring that it is near its previous position.
    If not identified, return the previous position.
    """
    px = int(prev_position[0])
    py = int(prev_position[1])
    small_img = img[(py-BEAD_SEARCH_RANGE):(py+BEAD_SEARCH_RANGE),(px-BEAD_SEARCH_RANGE):(px+BEAD_SEARCH_RANGE)]
    circles = cv2.HoughCircles(small_img, cv2.HOUGH_GRADIENT_ALT, 2, 30, param1=300, param2=0.85, minRadius=int(0.8 * DRIVEN_BEAD_RADIUS_IN_PIXELS))
    position = prev_position
    if circles is None:
        Warning('no beads detected')
    else:
        if show_circles:
            show_found_circles(small_img, circles)
        for circle in circles.reshape((circles.shape[0], circles.shape[-1])):
            if np.linalg.norm(circle[:-1] - [BEAD_SEARCH_RANGE,BEAD_SEARCH_RANGE]) < BEAD_MAX_DISPLACEMENT_IN_FRAME:
                position = circle[:-1]+[px-BEAD_SEARCH_RANGE, py-BEAD_SEARCH_RANGE]

    return position


def show_found_circles(img, circles):
    output = img.copy()
    for circle in circles:
        circle_rounded = np.round(circle.reshape(3)).astype("int")
        cv2.circle(output, (circle_rounded[0], circle_rounded[1]), circle_rounded[2], (0, 255, 0), 4)
    cv2.imshow('output', output)
    cv2.waitKey()


def improve_position(img, position):
    improved_position = position
    px = int(position[0])
    py = int(position[1])
    small_img = img[(py-BEAD_SEARCH_RANGE):(py+BEAD_SEARCH_RANGE),(px-BEAD_SEARCH_RANGE):(px+BEAD_SEARCH_RANGE)]
    circle = cv2.HoughCircles(small_img, cv2.HOUGH_GRADIENT_ALT, 2, 30, param1=300, param2=0.85, minRadius=params.DRIVEN_BEAD_RADIUS_IN_PIXELS / 2)
    if circle is not None:
        improved_position[0] = px-BEAD_SEARCH_RANGE+circle[0,0,0]
        improved_position[1] = py-BEAD_SEARCH_RANGE+circle[0,0,1]
    return improved_position


def remove_false_tracers(tracer_trajectories, frame_idx=None):
    """ remove noise by considering only tracers that are identified a large fraction of frames"""
    if frame_idx is None:
        frame_idx = max(len(history) for _, history in tracer_trajectories.items())
    delete_items = []
    if frame_idx < START_FILTERING:
        return
    for item in tracer_trajectories:
        trajectory = tracer_trajectories[item]
        first_detected = trajectory[0][0]
        if len(trajectory) < MIN_IDENTIFICATION_FRACTION*(frame_idx-first_detected):
            delete_items.append(item)
    for item in delete_items:
        tracer_trajectories.pop(item)


def plot_found_blobs(frame, positions):
    """ plots all of the found blobs, for diagnostic purposes"""
    plt.figure()
    plt.imshow(frame, cmap='gray')
    if isinstance(positions[0], float) or isinstance(positions[0], float): # in case we only get 1 position
        plt.scatter(positions[0], positions[1] , facecolors='none', edgecolors='b')
    else:
        plt.scatter([p[0] for p in positions], [p[1] for p in positions], facecolors='none', edgecolors='b')
    plt.show(block=False)


def get_sampling_rate(video_file):
    video_reader = cv2.VideoCapture()
    video_reader.open(str(video_file))
    return video_reader.get(cv2.CAP_PROP_FPS)


def find_oscillation_boundaries(trajectory, axis, n_noise_datapoints, plot=False):
    position = trajectory[:, axis]
    if DRIFT_FIX:
        reg = np.polyfit(np.arange(len(position)), position, 1)
        position = position - reg[0]*np.arange(len(position))+reg[1]

    first_peak = find_first_peak_center(position, n_noise_datapoints)
    last_peak = len(position)-1-find_first_peak_center(np.flip(position), n_noise_datapoints)
    if plot:
        plt.plot(position)
        plt.scatter(int(first_peak), position[int(first_peak)])
        plt.scatter(int(last_peak), position[int(last_peak)])
        plt.show(block=False)

    return first_peak, last_peak


def find_first_peak_center(signal: np.ndarray, n_noise_datapoints):
    """ find the index of the first maximum point in sinusoidal signal

    first, estimate where maximum is by a threshold.
    then, use a parabolic fit to find the peak center

     Args:
         signal : correspond to input of "get_boundaries_for_fourier" after smoothing
         threshold: a threshold for identification of peaks

     Returns:
         first_max_peak_index (int): index of first peak in signal
     """
    noise = np.std(signal[:n_noise_datapoints])
    base_level = np.average(signal[:n_noise_datapoints])
    gaussian = sg.gaussian(GAUSSIAN_LENGTH, SIGMA)

    if noise > 0:
        smooth_signal = base_level+np.convolve(signal-base_level, gaussian / sum(gaussian), 'same')
    else:
        smooth_signal = signal

    threshold = base_level+PEAK_DETECTION_THRESHOLD*(min(smooth_signal[:int(len(smooth_signal)/2)])-base_level)
    counter = 0
    while True:
        before_peak = np.argmax(smooth_signal < threshold)
        after_peak = before_peak + np.argmax(smooth_signal[before_peak:] > threshold)

        if after_peak-before_peak+1 < MIN_POINTS_IN_FIT:
            first_max_peak = (after_peak+before_peak)/2
            return first_max_peak
        else:
            parabolic_fit_parameters = np.polyfit(np.arange(before_peak, after_peak+1), signal[before_peak:after_peak+1],2)
            first_max_peak = -parabolic_fit_parameters[1] / 2 / parabolic_fit_parameters[0]
            peak_value = parabolic_fit_parameters[2]-parabolic_fit_parameters[1]**2/4/parabolic_fit_parameters[0]
            return first_max_peak
        if counter > MAX_ITER:
            raise RuntimeError('smoothing failed - cannot identify peaks')
        counter = counter+1

if __name__ == '__main__':
    tracer_trajectories = record_tracer_trajectories_trackpy(VIDEO_PATH, quiet=False)