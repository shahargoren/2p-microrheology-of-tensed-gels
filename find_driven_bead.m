clear all
restoredefaultpath
addpath(genpath([pwd,'/methods']))
global n_frames
paths = {...
        'D:\Shahar\Shahar Optical Tweezers\sample videos\'
        };
input_positions = false;
compute_trajectories = false;
save_trajectories = false;
save_radii = false;

for k = 1:length(paths)
    path = paths{k};
    files = dir(path);
    %% have the user mark the driven bead neighborhood and position
    for i = 1:length(files)
        file = files(i);
        [file_is_video, type] = is_video_file(file.name);
        if file_is_video
            user_input_positions_file = [path, file.name(1:end-length(type)-1), ' positions.mat'];
            if input_positions || ~isfile(user_input_positions_file)
                disp(file.name)
                first_frame = get_video_frame(file, 1, type);
                first_frame = pretracking_processing(first_frame);
                figure(1); clf; hold on;
                imshow(first_frame)
                rectangle = floor(getrect);
                close(1)
                small_img = first_frame(rectangle(2):rectangle(2)+rectangle(4),rectangle(1):rectangle(1)+rectangle(3));
                figure(1); clf; hold on;
                imshow(small_img);
                [px, py] = ginput(1);
                [pos_x, pos_y, polarity, r] = find_circle_close_to_position(small_img, px, py, 'unknown');
                close(1)
                save(user_input_positions_file,'rectangle', 'pos_x', 'pos_y', 'polarity')
            end
        end
    end
end
%% track the bead in each video
for k = 1:length(paths)
    path = paths{k};
    files = dir(path);
    for i = 1:length(files)
        file = files(i);
        [file_is_video, type] = is_video_file(file.name);
        if file_is_video
            trajectory_file = [path, file.name(1:end-length(type)-1), ' trajectory.csv'];
            radius_file = [path, file.name(1:end-length(type)-1), ' bead_radius.csv'];
            in_process_file = [path, file.name(1:end-length(type)-1), ' in_process.txt'];
            done_file = [path, file.name(1:end-length(type)-1), ' done.txt'];
            if (compute_trajectories || ~isfile(trajectory_file) || ~isfile(radius_file)) ...
                    && ~isfile(in_process_file) && ~isfile(done_file)
                writematrix([1,2,3], in_process_file)
                if strcmp(type, 'tiff')
                    n_frames = length(imfinfo([file.folder, '/', file.name]));
                end
                disp(file.name)
                user_input_positions_file = [path, file.name(1:end-length(type)-1), ' positions.mat'];
                frame_ind = 2;
                load(user_input_positions_file)
                while true
                    if mod(frame_ind, 50) == 0
                        disp(['frame ', num2str(frame_ind)]);
                    end
                    frame = get_video_frame(file, frame_ind, type);
                    if isnan(frame)
                        break
                    end
                    frame = pretracking_processing(frame);
                    small_img = frame(rectangle(2):rectangle(2)+rectangle(4),rectangle(1):rectangle(1)+rectangle(3));
                    px = pos_x(frame_ind-1);
                    py = pos_y(frame_ind-1);
                    [px, py, ~, r] = find_circle_close_to_position(small_img, px, py, polarity);
                    pos_x(frame_ind) = px;
                    pos_y(frame_ind) = py;
                    bead_radius(frame_ind) = r;
                    frame_ind = frame_ind+1;
                end
                pos_x = pos_x+rectangle(1);
                pos_y = pos_y+rectangle(2);
                mean_bead_radius = nanmean(bead_radius);
                if save_radii || ~isfile(radius_file)
                    writematrix(mean_bead_radius, radius_file)
                end
                if save_trajectories || ~isfile(trajectory_file)
                    writematrix([pos_x', pos_y'], trajectory_file)
                end
                writematrix([1,2,3], done_file)
                delete(in_process_file)
            end
        end
    end
    in_process_files = dir([path,'* in_process.txt']);
    if isempty(in_process_files)
        done_files = dir([path,'* done.txt']);
        for i = 1:length(done_files)
            delete([done_files(i).folder,'\',done_files(i).name])
        end
    end
end




